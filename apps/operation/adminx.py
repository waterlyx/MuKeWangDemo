# _*_ encoding:utf-8 _*_

__author__ = 'liuyexing'
__date__ = '2017/11/21 21:29'

import xadmin

from .models import UserAsk, UserCourse, UserFavourite, CourseComment, UserMessage


class UserAskAdmin(object):
    list_display = ['name', 'mobile', 'course_name', 'add_time']
    search_filter = ['name', 'mobile', 'course_name']
    list_filter = ['name', 'mobile', 'course_name', 'add_time']


class CourseCommentAdmin(object):
    list_display = ['user', 'course', 'comments', 'add_time']
    search_filter = ['user', 'course', 'comments']
    list_filter = ['user__nick_name', 'course__name', 'comments', 'add_time']


class UserFavouriteAdmin(object):
    list_display = ['user', 'fav_id', 'fav_type', 'add_time']
    search_filter = ['user', 'fav_id', 'fav_type']
    list_filter = ['user__nick_name', 'fav_id', 'fav_type', 'add_time']


class UserCourseAdmin(object):
    list_display = ['user', 'course', 'add_time']
    search_filter = ['user', 'course']
    list_filter = ['user__nick_name', 'course', 'add_time']


class UserMessageAdmin(object):
    list_display = ['user', 'message', 'has_read', 'add_time']
    search_filter = ['user', 'message', 'has_read']
    list_filter = ['user', 'message', 'has_read', 'add_time']


xadmin.site.register(UserAsk, UserAskAdmin)
xadmin.site.register(CourseComment, CourseCommentAdmin)
xadmin.site.register(UserFavourite, UserFavouriteAdmin)
xadmin.site.register(UserCourse, UserCourseAdmin)
xadmin.site.register(UserMessage, UserMessageAdmin)
